import argparse
import json
from nvd_api_v2 import ANSI_CODES, NVD_API


class CPE_API(NVD_API):
    """Download and manage CPEs."""
    def __init__(self, gitlab_url, gitlab_token):
        """ Create a new API endpoint."""
        NVD_API.__init__(self, 'cpes', gitlab_url, gitlab_token, 'meta/nvdcpe-2.0-meta.json')
        self.cpes_dropped = set()
        self.cpes_changed = dict()

    def save(self, timestamp, content):
        """
        Save the response of a single API request to files.
        """
        modified = 0
        dropped = 0

        for product in content['products']:
            cpe = product['cpe']
            cpe_part = cpe['cpeName'].split(':')[2]
            # we are not interested CPEs that are for operating systems (o)
            # or hardware (h)
            if cpe['deprecated'] or cpe_part != 'a':
                self.cpes_dropped.add(cpe['cpeNameId'])
                dropped = dropped + 1
                continue

            modified = modified + 1
            self.cpes_changed[cpe['cpeNameId']] = cpe['cpeName']

        print(f'{ANSI_CODES.GREEN}{modified}{ANSI_CODES.RESET} added/changed, {ANSI_CODES.RED}{dropped}{ANSI_CODES.RESET} dropped.')

    def generate_partials(self, cpes):
        self.print_step('Generating partials')
        partials = set()

        for cpe_id, cpe_name in cpes.items():
            partials.add(":".join(cpe_name.split(":")[:5]))

        return sorted(partials)

    def update(self):
        self.load_meta()
        self.print_step('Fetching new CPEs')
        self.download()

        self.print_step('Fetching existing files')
        cpes = self.cpes_changed
        existing_cpes = self.gitlab_download(f'{self.gitlab_url}/latest/nvdcpe-2.0-data.json.gz', True)
        if existing_cpes:
            cpes = existing_cpes | cpes

        for cpe_id in self.cpes_dropped:
            if cpe_id in cpes:
                cpes.pop(cpe_id)

        partials = self.generate_partials(cpes)

        self.print_step('Uploading new files')
        self.gitlab_upload(json.dumps(cpes), f'{self.gitlab_url}/latest', 'nvdcpe-2.0-data.json.gz', True)
        self.gitlab_upload(json.dumps(partials), f'{self.gitlab_url}/latest', 'nvdcpe-2.0-partials-data.json.gz', True)
        self.gitlab_upload(json.dumps({'last_update': self.last_update}), f'{self.gitlab_url}/latest', 'nvdcpe-2.0-meta.json')

        self.save_meta()
        self.print_step('done')


def __main__():
    parser = argparse.ArgumentParser('NVD CPE API fetcher')
    parser.add_argument('-u', '--gitlab-url', required=True,
                        help='The Gitlab API endpoint for package uploads.')
    parser.add_argument('-t', '--gitlab-token', required=True,
                        help='A Gitlab job token for package upload.')
    args = parser.parse_args()

    api = CPE_API(args.gitlab_url, args.gitlab_token)
    api.update()


__main__()
