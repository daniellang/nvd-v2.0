from datetime import datetime, timedelta, timezone
import gzip
import json
import requests
import time

NVD_API_VERSION = '2.0'
NVD_API_BASE_URL = 'https://services.nvd.nist.gov/rest/json'
# NVD rate limiting allows 5 requests per 30 seconds -> one every 6 seconds.
NVD_SLEEP_TIME_SEC = 30 / 5
# Number of minutes that are subtracted from the update timestap given by NVD.
NVD_TIMESTAMP_SUBTRACT_MINUTES = 1
# Only update the database if the given number of seconds elapsed.
NVD_CHECK_TIMEOUT_SECONDS = 120 * 60


class ANSI_CODES:
    RESET = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    CYAN = '\033[36m'


class NVD_API:
    """A helper class that fetches data from a NVD API.

    This class defines two functions that need to be implemented in a derived class.
    save Called to actually analyse and save the downloaded data."""

    def __init__(self, service, gitlab_url, gitlab_token, meta_filename):
        """Initialize a new NVD API endpoint.

        service       - The service that gets appended to the API URL, should be 'cves' or 'cpes'.
        gitlab_path   - Gitlab package registry path without package version.
        gitlab_token  - Gitlab token that is able to upload to the package registry.
        meta_filename - Name of the meat file (e.g. nvdcve-2.0-meta.json).
        """
        self.service = service
        self.gitlab_url = gitlab_url
        self.gitlab_token = gitlab_token
        self.meta_filename = meta_filename
        self.last_update = None
        self.url = f'{NVD_API_BASE_URL}/{self.service}/{NVD_API_VERSION}'

    def save(self, timestamp, content):
        """Used to save the data given by a single API request.

        timestamp is the last update timestamp given by NVD.
        content is the json downloaded from NVD.
        Needs to be implemented by derived classes.
        """
        pass

    def download(self):
        """Download all entries from NVD since last_update (if not None).

        For each downloaded page save_to_db is called.
        """
        args = {}
        start_index = 0
        total_results = 0
        results_per_page = 0

        if (self.last_update is not None):
            args['lastModStartDate'] = self.last_update.isoformat()
            args['lastModEndDate'] = datetime.now(tz=timezone.utc).isoformat()

        while True:
            args['startIndex'] = start_index
            print(f'Downloading {self.url} - params: {args}')

            page = requests.get(self.url, params=args)
            page.raise_for_status()
            content = page.json()

            if content is None:
                print(f'{ANSI_CODES.BOLD}{ANSI_CODES.RED}Nothing was downloaded{ANSI_CODES.RESET}')
                return

            results_per_page = content['resultsPerPage']
            total_results = content['totalResults']
            start_index = content['startIndex']
            timestamp = content['timestamp']

            start_index += results_per_page

            # Call the save method of the derived class
            self.save(timestamp, content)

            print(f'[{start_index:0{len(str(total_results))}}/{total_results}]')

            if start_index >= total_results:
                # Everything has been downloaded
                self.last_update = timestamp
                return

            # Otherwise rate limiting will be hit.
            time.sleep(NVD_SLEEP_TIME_SEC)

    def load_meta(self):
        self.print_step('Fetching meta file')
        meta = self.gitlab_download(f'{self.gitlab_url}/{self.meta_filename}')

        if meta:
            self.last_update = datetime.fromisoformat(meta['last_update']) - timedelta(minutes=NVD_TIMESTAMP_SUBTRACT_MINUTES)

    def save_meta(self):
        self.print_step('Uploading meta file')
        self.gitlab_upload(json.dumps({'last_update': self.last_update}), self.gitlab_url, self.meta_filename)

    def gitlab_download(self, gitlab_url, compressed=False) -> bool:
        print(f'Downloading {gitlab_url}')
        page = requests.get(gitlab_url)

        if page.status_code != requests.codes.ok:
            print(f'{ANSI_CODES.RED}Download failed!{ANSI_CODES.RESET}')
            return None

        data = page.content
        if compressed:
            data = gzip.decompress(data)

        return json.loads(data.decode('utf-8'))

    def gitlab_upload(self, content, gitlab_url, filename, compressed=False):
        data = content.encode('utf-8')

        if compressed:
            data = gzip.compress(data)

        print(f'Uploading {gitlab_url}/{filename}')
        page = requests.put(f'{gitlab_url}/{filename}', headers={'JOB-TOKEN': self.gitlab_token}, data=data)
        page.raise_for_status()

    def print_step(self, step):
        print(f'{ANSI_CODES.BOLD}{ANSI_CODES.CYAN}{step}{ANSI_CODES.RESET}')
