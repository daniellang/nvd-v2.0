import argparse
import json
from nvd_api_v2 import ANSI_CODES, NVD_API


class CVE_API(NVD_API):
    """Download and manage CVEs."""
    def __init__(self, gitlab_url, gitlab_token):
        """ Create a new API endpoint."""
        NVD_API.__init__(self, 'cves', gitlab_url, gitlab_token, 'meta/nvdcve-2.0-meta.json')
        self.last_update_year = dict()
        self.cves_dropped = set()
        self.cve_year = dict()

    def save(self, timestamp, content):
        """
        Save the response of a single API request to files.
        """
        modified = 0
        dropped = 0

        for vulnerability in content['vulnerabilities']:
            if vulnerability['cve']['vulnStatus'] == 'Rejected' or 'configurations' not in vulnerability['cve']:
                self.cves_dropped.add(vulnerability['cve']['id'])
                dropped = dropped + 1
                continue

            modified = modified + 1
            cve_id = vulnerability['cve']['id']
            cve_id_parts = cve_id.split('-')
            cve_id_year = cve_id_parts[1]

            # Copy the fields that we are interested in
            cve = dict()
            cve['lastModified'] = vulnerability['cve']['lastModified']
            cve['configurations'] = vulnerability['cve']['configurations']

            self.last_update_year[cve_id_year] = timestamp

            if cve_id_year not in self.cve_year:
                self.cve_year[cve_id_year] = dict()

            self.cve_year[cve_id_year][cve_id] = cve

        print(f'{ANSI_CODES.GREEN}{modified}{ANSI_CODES.RESET} added/changed, {ANSI_CODES.RED}{dropped}{ANSI_CODES.RESET} dropped.')

    def update(self):
        self.load_meta()
        self.print_step('Fetching new CVEs')
        self.download()

        self.print_step('Downloading existing files / uploading new files')
        for year, cves in self.cve_year.items():
            existing_cves = self.gitlab_download(f'{self.gitlab_url}/{year}/nvdcve-2.0-{year}-data.json.gz', True)
            if existing_cves:
                cves = existing_cves | cves

            for cve_id in self.cves_dropped:
                if cve_id in cves:
                    cves.pop(cve_id)

            self.gitlab_upload(json.dumps(cves), f'{self.gitlab_url}/{year}', f'nvdcve-2.0-{year}-data.json.gz', True)

        self.print_step('Uploading meta file per year')
        for year, timestamp in self.last_update_year.items():
            self.gitlab_upload(json.dumps({'last_update': timestamp}), f'{self.gitlab_url}/{year}', f'nvdcve-2.0-{year}-meta.json')

        self.save_meta()
        self.print_step('done')


def __main__():
    parser = argparse.ArgumentParser('NVD CVE API fetcher')
    parser.add_argument('-u', '--gitlab-url', required=True,
                        help='The Gitlab API endpoint for package uploads.')
    parser.add_argument('-t', '--gitlab-token', required=True,
                        help='A Gitlab job token for package upload.')
    args = parser.parse_args()

    api = CVE_API(args.gitlab_url, args.gitlab_token)
    api.update()


__main__()
